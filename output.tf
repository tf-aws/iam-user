output "username" {
  description = "The user's name"
  value       = element(concat(aws_iam_user.default.*.name, [""]), 0)
}

output "arn" {
  description = "The ARN assigned by AWS for default user"
  value       = element(concat(aws_iam_user.default.*.arn, [""]), 0)
}

output "unique_id" {
  description = "The unique ID assigned by AWS"
  value       = element(concat(aws_iam_user.default.*.unique_id, [""]), 0)
}

output "access_key_id" {
  description = "The access key ID"
  value = element(
    concat(
      aws_iam_access_key.default.*.id,
      [""],
    ),
    0,
  )
}

output "access_key_secret" {
  description = "The access key secret"
  value       = element(concat(aws_iam_access_key.default.*.secret, [""]), 0)
}