resource "aws_iam_user" "default" {
  count                = var.create_user ? 1 : 0
  name                 = var.name
  path                 = var.path
  force_destroy        = var.force_destroy
  permissions_boundary = var.permissions_boundary
  tags                 = var.tags
}

resource "aws_iam_access_key" "default" {
  count = var.create_user && var.create_iam_access_key ? 1 : 0
  user  = aws_iam_user.default[0].name
}
